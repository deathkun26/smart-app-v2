import { compareTwoTexts } from "text-sound-similarity";

import axios from "axios";

const AIO_USERNAME = "thkgq123";
const AIO_KEY = "aio_cGdw29jsFz6jc0uKK9zOCJapdCl8";
const TRIGGER_TEXT = [
    "turn on light one",
    "turn on light two",
    "turn on light three",
    "turn off light one",
    "turn off light two",
    "turn off light three",
];

const headerSend = {
    headers: {
        "X-AIO-Key": AIO_KEY,
        "Content-Type": "application/json",
    },
};

export async function Trigger(texts) {
    const triggerNum = getTrigger(texts);
    var response;
    switch (triggerNum) {
        case 0:
            response = await axios.post(
                `https://io.adafruit.com/api/v2/${AIO_USERNAME}/feeds/iot-led-1/data`,
                {
                    datum: {
                        value: "0",
                    },
                },
                headerSend
            );
            break;
        case 1:
            response = await axios.post(
                `https://io.adafruit.com/api/v2/${AIO_USERNAME}/feeds/iot-led-2/data`,
                {
                    datum: {
                        value: "2",
                    },
                },
                headerSend
            );
            break;
        case 2:
            response = await axios.post(
                `https://io.adafruit.com/api/v2/${AIO_USERNAME}/feeds/iot-led-3/data`,
                {
                    datum: {
                        value: "4",
                    },
                },
                headerSend
            );
            break;
        case 3:
            response = await axios.post(
                `https://io.adafruit.com/api/v2/${AIO_USERNAME}/feeds/iot-led-1/data`,
                {
                    datum: {
                        value: "1",
                    },
                },
                headerSend
            );
            break;
        case 4:
            response = await axios.post(
                `https://io.adafruit.com/api/v2/${AIO_USERNAME}/feeds/iot-led-2/data`,
                {
                    datum: {
                        value: "3",
                    },
                },
                headerSend
            );
            break;
        case 5:
            response = await axios.post(
                `https://io.adafruit.com/api/v2/${AIO_USERNAME}/feeds/iot-led-3/data`,
                {
                    datum: {
                        value: "5",
                    },
                },
                headerSend
            );
            break;
        default:
            console.log("Cannot recognize command");
    }
    console.log(response);
}

function getTrigger(texts) {
    var maxSimilar = 0;
    var trigger;
    texts.forEach((text) =>
        TRIGGER_TEXT.forEach((trigger_text, index) => {
            if (compareTwoTexts(text, trigger_text) > maxSimilar) {
                maxSimilar = compareTwoTexts(text, trigger_text);
                trigger = index;
            }
        })
    );
    if (maxSimilar >= 0.8) {
        return trigger;
    } else {
        return -1;
    }
}
