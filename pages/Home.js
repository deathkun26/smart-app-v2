import {
    ActivityIndicator, StyleSheet, View, Text, SafeAreaView, Platform,
    StatusBar, FlatList, TouchableOpacity, ImageBackground, Image, TouchableHighlight
} from 'react-native';

import { Trigger } from '../light-trigger/Main';

import React, { useEffect, useState } from 'react';

import Voice from '@react-native-voice/voice';

import axios from 'axios';

const AIO_FEED_IDS = ["proj-led", "proj-light"];
const AIO_USERNAME = "thkgq123";
const AIO_KEY = "aio_cGdw29jsFz6jc0uKK9zOCJapdCl8";

const numDevices = 3;

let isRecord = false;

export function HomeScreen({ route, navigation }) {
    const { canUseVoice, listDevice } = route.params;
    const [isLoading, setLoading] = useState(true);
    const [device, setDevice] = useState([]);
    const [result, setResult] = useState('');
    const [buttonColor, setButtonColor] = useState('#1760FC');
    const [history, setHistory] = useState(['', '', '']);

    // Start voice implementation

    const onSpeechStartHandler = (e) => {
        console.log("start handler==>>>", e);
    }
    const onSpeechEndHandler = (e) => {
        console.log("stop handler", e);
        stopRecording();
    }

    const onSpeechResultsHandler = async (e) => {
        let text = e.value[0];
        setResult(text);
        console.log("speech result handler", e);
        stopRecording();
        await Trigger(e.value);
        await getDevice();
    }

    const startRecording = async () => {
        isRecord = true;
        setButtonColor('#FE4A49');
        console.log("Start recording");
        try {
            await Voice.start('en-US');
        } catch (error) {
            console.log("error raised", error);
        }
    }

    const stopRecording = async () => {
        isRecord = false;
        setButtonColor('#407DFF');
        console.log("Stop recording");
        try {
            await Voice.stop();
        } catch (error) {
            console.log("error raised", error);
        }
    }

    const buttonHandle = async () => {
        if (!isRecord) {
            startRecording();
        }
        else {
            stopRecording();
        }

    }

    // End voice implementation

    const updateHistory = () => {
        let newHistory = [];
        newHistory[2] = history[1];
        newHistory[1] = history[0];
        newHistory[0] = result;
        setHistory(newHistory);
    }

    const renderItem = ({ item }) => (
        <View style={styles.item}>
            {item.data === "ON" ?
                <Image
                    source={require('../assets/light_on.png')}
                /> :
                <Image
                    source={require('../assets/light_off.png')}
                />}
            <Text style={styles.itemText}>{item.name}</Text>
        </View>
    );

    const getDevice = async () => {
        try {
            let devices = [];
            for (let i = 1; i <= numDevices; i++) {
                let new_device = {};
                const response = await axios.get(
                    `https://io.adafruit.com/api/v2/${AIO_USERNAME}/feeds/iot-led-${i}`
                );
                new_device.key = `iot-led-${i}`;
                new_device.id = response.data.id;
                new_device.name = response.data.name;
                new_device.data = response.data.last_value % 2 == 0 ? 'ON' : 'OFF';
                //console.log(device);
                devices.push(new_device);
            }
            //console.log(devices);
            setDevice(devices);
        }
        catch (error) {
            console.log(error);
        }
    }


    useEffect(() => {
        console.log("Home page");

        Voice.onSpeechStart = onSpeechStartHandler;
        Voice.onSpeechEnd = onSpeechEndHandler;
        Voice.onSpeechResults = onSpeechResultsHandler;

        let interval = null;
        if (isLoading) {
            interval = setInterval(() => getDevice(), 500);
            setLoading(false);
        }

        return () => {
            clearInterval(interval);
            Voice.destroy().then(Voice.removeAllListeners);
        }
    }, []);

    useEffect(() => {
        updateHistory();
    }, [result])

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.body}>
                {isLoading ? <ActivityIndicator /> : (
                    <FlatList
                        horizontal={true}
                        alignItems="center"
                        justifyContent="center"
                        data={device.length == 0 ? listDevice : device}
                        keyExtractor={item => item.id}
                        renderItem={renderItem}
                    />
                )}
            </View>
            <View style={styles.footer}>
                <View style={styles.history}>
                    <Text style={styles.historyTextThird}>{history[2]}</Text>
                    <Text style={styles.historyTextSecond}>{history[1]}</Text>
                    <Text style={styles.historyTextFirst}>{history[0]}</Text>
                </View>
                {canUseVoice == true ?
                    <>
                        <View style={{ flexDirection: 'row', margin: 30 }}>
                            <TouchableOpacity
                                onPress={buttonHandle}
                                style={[styles.roundButton, { backgroundColor: buttonColor }]}>
                                <Image source={require("../assets/voice.png")} style={{ height: 40, width: 20 }}></Image>
                            </TouchableOpacity>
                        </View>
                        <View >
                            {isRecord == false ?
                                <Text>Bấm vào nút Record để bắt đầu thu lệnh</Text> :
                                <Text>Hãy nói gì đó ...</Text>}
                        </View>
                    </> :
                    <View>
                        <Text style={{ color: '#FE4A49' }}>Vui lòng cho phép truy cập Microphone</Text>
                    </View>}

            </View>
        </SafeAreaView >
    );
}

const styles = StyleSheet.create({
    container: {
        fontFamily: "Roboto",
        flex: 1,
        backgroundColor: '#1760FC',
    },
    body: {
        flex: 2,
        width: '100%',
        backgroundColor: 'white',
        justifyContent: 'center',
    },
    item: {
        padding: 20,
    },
    itemText: {
        marginTop: 10,
        fontWeight: 'bold',
    },
    footer: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 3,
    },
    roundButton: {
        width: 60,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        borderRadius: 100,
    },
    history: {
        textAlign: 'center',
        alignItems: 'center',
    },
    historyTextFirst: {
        marginTop: 10,
        fontSize: 20,
    },
    historyTextSecond: {
        margin: 10,
        fontSize: 17,
        opacity: 0.5,
    },
    historyTextThird: {
        margin: 10,
        fontSize: 15,
        opacity: 0.2,
    }
});


